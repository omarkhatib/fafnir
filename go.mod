module codeberg.org/omarkhatib/fafnir

go 1.17

require (
	github.com/cavaliercoder/grab v2.0.0+incompatible
	github.com/mattn/go-sqlite3 v1.14.9
)
